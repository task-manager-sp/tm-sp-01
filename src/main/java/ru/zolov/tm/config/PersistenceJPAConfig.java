package ru.zolov.tm.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan("ru.zolov.tm")
@EnableJpaRepositories(basePackages = "ru.zolov.tm.api")
@EnableTransactionManagement(proxyTargetClass = true)
public class PersistenceJPAConfig {

  @Value("${dialect}")
  private String dialect;
  @Value("${url}")
  private String url;
  @Value("${login}")
  private String username;
  @Value("${password}")
  private String password;
  @Value("${driver}")
  private String driver;
  @Value("tableStrategy")
  private String tableStrategy;

  @Bean public DataSource dataSource() {
    final DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(driver);
    dataSource.setUrl(url);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    return dataSource;
  }

  @Bean public LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean factoryBean;
    factoryBean = new LocalContainerEntityManagerFactoryBean();
    factoryBean.setDataSource(dataSource);
    factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    factoryBean.setPackagesToScan("ru.zolov.tm");
    Properties properties = new Properties();
    properties.put("hibernate.show.sql", "true");
    properties.put("hibernate.hdm2ddl.auto", tableStrategy);
    properties.put("hibernate.dialect", dialect);
    factoryBean.setJpaProperties(properties);
    return factoryBean;
  }

  @Bean public PlatformTransactionManager transactionManager(final LocalContainerEntityManagerFactoryBean emf) {
    final JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(emf.getObject());
    return transactionManager;
  }
}
