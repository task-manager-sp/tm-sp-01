package ru.zolov.tm.service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import ru.zolov.tm.entity.AbstractEntity;
import ru.zolov.tm.entity.Project;

public abstract class AbstractService {

  public SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
}
